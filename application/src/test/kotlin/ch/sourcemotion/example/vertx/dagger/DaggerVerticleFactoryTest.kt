package ch.sourcemotion.example.vertx.dagger

import dagger.Component
import io.vertx.core.Vertx
import io.vertx.core.eventbus.Message
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.RunTestOnContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import io.vertx.kotlin.coroutines.awaitResult
import kotlinx.coroutines.experimental.launch
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Test to demonstrate an example how to change the Dagger graph for testing purposes.
 */
@RunWith(VertxUnitRunner::class)
class DaggerVerticleFactoryTest {

    @JvmField
    @Rule
    val vertxContext = RunTestOnContext()

    lateinit var testInit: VerticleFactoryTestInit

    @Before
    fun setUp() {
        testInit = DaggerVerticleFactoryTestComponent.builder()
                .testVertxModule(TestVertxModule(vertxContext.vertx()))
                .testDaggerVerticleModule(TestDaggerVerticleModule)
                .daggerVerticleFactoryModule(DaggerVerticleFactoryModule).build().testInit()
    }

    /**
     * Test that a daggerized verticle can be deployed and it proceeds events.
     */
    @Test(timeout = 5000)
    fun callDaggerizedVerticle(context: TestContext) {
        testInit.initCallDaggerizedVerticle()

        val vertx = vertxContext.vertx()

        val async = context.async()

        launch {
            // Deploy daggerized test verticle
            awaitResult<String> { vertx.deployVerticle("dagger:${TestDaggerVerticle::class.java.name}", it) }

            // Send event to the verticle and verify returned value
            val expectedValue = Random().nextInt()
            val receivedValue = awaitResult<Message<Int>> { vertx.eventBus().send(TestDaggerVerticle.ADDRESS, expectedValue, it) }.body()
            context.assertEquals(expectedValue, receivedValue)

            async.complete()
        }
    }
}

/**
 * This testing component uses the test modules for vertx which relies on the [Rule] controlled Vert.x instance.
 * Additionally the module to provide the [TestDaggerVerticle] to the factory.
 */
@Singleton
@Component(modules = [TestVertxModule::class, TestDaggerVerticleModule::class, DaggerVerticleFactoryModule::class])
interface VerticleFactoryTestComponent {
    fun testInit(): VerticleFactoryTestInit
}


class VerticleFactoryTestInit @Inject constructor(private val vertx: Vertx) {
    fun initCallDaggerizedVerticle() {
        // No-op
    }
}
