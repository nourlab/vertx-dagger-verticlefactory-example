package ch.sourcemotion.example.vertx.dagger

import io.vertx.core.eventbus.Message
import io.vertx.kotlin.coroutines.CoroutineVerticle

/**
 * Simple test reply verticle
 */
class TestDaggerVerticle : CoroutineVerticle() {

    companion object {
        const val ADDRESS = "test-address"
    }

    override suspend fun start() {
        vertx.eventBus().consumer(ADDRESS, this::onMessage)
    }

    private fun onMessage(msg: Message<Int>) {
        msg.reply(msg.body())
    }
}