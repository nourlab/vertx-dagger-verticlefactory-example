package ch.sourcemotion.example.vertx.dagger

import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import dagger.multibindings.StringKey
import io.vertx.core.Verticle

/**
 * Module to provide [TestDaggerVerticle] instance
 */
@Module
object TestDaggerVerticleModule {

    @Provides
    @IntoMap
    @StringKey("ch.sourcemotion.example.vertx.dagger.TestDaggerVerticle")
    fun provideFruitVerticle(): Verticle = TestDaggerVerticle()
}