package ch.sourcemotion.example.vertx.dagger

import dagger.Module
import dagger.Provides
import io.vertx.core.Vertx
import io.vertx.core.spi.VerticleFactory
import javax.inject.Singleton

@Module
object VertxModule {

    /**
     * Provides the global [Vertx] instance to use. The Dagger verticle factory will be registered here, so
     * we get an ready to use Vert.x instance from here on.
     */
    @Provides
    @Singleton
    fun provideVertx(verticleFactory: VerticleFactory): Vertx {
        val vertx = Vertx.vertx()
        vertx.registerVerticleFactory(verticleFactory)
        return vertx
    }
}