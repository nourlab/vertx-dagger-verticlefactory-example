package ch.sourcemotion.example.vertx.dagger

import dagger.Component
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.kotlin.coroutines.awaitResult
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.experimental.runBlocking
import org.slf4j.LoggerFactory
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

fun main(args: Array<String>) {
    DaggerApplicationComponent.builder()
            .fruitStoreModule(FruitStoreModule)
            .vertxModule(VertxModule)
            .daggerVerticleFactoryModule(DaggerVerticleFactoryModule)
            .fruitVerticleModule(FruitVerticleModule)
            .storePrintVerticleModule(StorePrintVerticleModule)
            .build()
            .application().start()
}

/**
 * The application itself.
 */
class Application @Inject constructor(private val vertx: Vertx) {

    private val log = LoggerFactory.getLogger(Application::class.java)

    fun start() {
        runBlocking(vertx.dispatcher()) {
            startFruitVerticles()
            startStorePrintVerticle()

            sendAddFruitsEvents()
            sendPrintEvents()
        }
    }

    /**
     * Deployment of the verticle that print the fruit in the store
     */
    private suspend fun startStorePrintVerticle() {
        awaitResult<String> { vertx.deployVerticle("dagger:${StorePrintVerticle::class.java.name}", it) }
        log.info("Store print verticle deployed successful")
    }

    /**
     * Deployment of the fruit verticle. A verticle per [Fruit]
     */
    private suspend fun startFruitVerticles() {
        awaitResult<String> { vertx.deployVerticle("dagger:${FruitVerticle::class.java.name}", DeploymentOptions().setInstances(Fruit.values().size), it) }
        log.info("Fruit verticles deployed successful")
    }

    /**
     * Sends a print event in a period of 4 seconds
     */
    private fun sendPrintEvents() {
        log.info("Start sending print fruits in store event")
        vertx.setPeriodic(4000) {
            vertx.eventBus().send(StorePrintVerticle.ADDRESS, null)
        }
    }

    /**
     * Sends a add fruits event in a period of 3 seconds
     */
    private fun sendAddFruitsEvents() {
        log.info("Start publishing add fruits event")
        val random = Random()
        vertx.setPeriodic(3000) {
            // Send an amount to each fruit verticle to store
            vertx.eventBus().send(FruitVerticle.buildAddress(Fruit.APPLE), random.nextInt(5))
            vertx.eventBus().send(FruitVerticle.buildAddress(Fruit.RASPBERRY), random.nextInt(5))
            vertx.eventBus().send(FruitVerticle.buildAddress(Fruit.BANANA), random.nextInt(5))
        }
    }
}

/**
 * Component which configures all needed Dagger modules to run the application.
 */
@Singleton
@Component(modules = [VertxModule::class, DaggerVerticleFactoryModule::class, FruitStoreModule::class,
    FruitVerticleModule::class, StorePrintVerticleModule::class])
interface ApplicationComponent {
    fun application(): Application
}