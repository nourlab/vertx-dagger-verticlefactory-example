package ch.sourcemotion.example.vertx.dagger

import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import dagger.multibindings.StringKey
import io.vertx.core.Verticle

/**
 * One of the most central parts on this approach. The dagger verticle factory will delegate the instance creation to
 * the here implemented provider method.
 */
@Module
object FruitVerticleModule {

    private val fruitIter = Fruit.values().iterator()

    /**
     * Provides [FruitVerticle] instances.
     * Will be called each time the [ch.sourcemotion.example.vertx.dagger.DaggerVerticleFactory] need's a further instance.
     */
    @Provides
    @IntoMap
    @StringKey("ch.sourcemotion.example.vertx.dagger.FruitVerticle")
    fun provideFruitVerticle(store: FruitStore): Verticle = FruitVerticle(store, fruitIter.next())
}