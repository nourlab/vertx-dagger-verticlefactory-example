package ch.sourcemotion.example.vertx.dagger

import io.vertx.core.eventbus.Message
import io.vertx.kotlin.coroutines.CoroutineVerticle
import kotlinx.coroutines.experimental.launch
import org.slf4j.LoggerFactory

/**
 * Verticle that get instantiated by a "Daggerized" [javax.inject.Provider], so complex object's can be used as instance
 * variables, instead of just json configuration
 */
class FruitVerticle(private val store: FruitStore, private val fruit: Fruit) : CoroutineVerticle() {

    private val log = LoggerFactory.getLogger(FruitVerticle::class.java)

    companion object {
        fun buildAddress(fruit: Fruit): String = "add-${fruit.name}.cmd"
    }

    override suspend fun start() {
        vertx.eventBus().consumer<Int>(buildAddress(fruit), this::onAddFruitEvent)
        log.info("${fruit.name} verticle started")
    }

    /**
     * When event received the given count of fruit will be added to store.
     */
    private fun onAddFruitEvent(msg: Message<Int>) {
        launch {
            val count = msg.body()
            for (i in 1..count) {
                store.storeFruits(fruit, count)
            }
            log.info("$count ${fruit.name} added to store")
        }
    }
}