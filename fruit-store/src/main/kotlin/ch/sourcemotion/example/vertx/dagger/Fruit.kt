package ch.sourcemotion.example.vertx.dagger

/**
 * Just a couple of fruits
 */
enum class Fruit {
    APPLE, RASPBERRY, BANANA
}