package ch.sourcemotion.example.vertx.dagger

import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap

/**
 * Just a very simple demo in-memory store
 */
open class InMemoryFruitStore : FruitStore {
    private val log = LoggerFactory.getLogger(InMemoryFruitStore::class.java)

    private val fruits: MutableMap<Fruit, Int> = ConcurrentHashMap()

    init {
        log.info("In-memory fruit store instantiated")
    }

    override fun storeFruits(fruit: Fruit, count: Int) {
        fruits[fruit] = fruits.getOrDefault(fruit, 0).plus(count)
    }

    override fun getFruitCount(fruit: Fruit): Int = fruits.getOrDefault(fruit, 0)
}

